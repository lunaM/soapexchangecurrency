package com.epam.exchange.currency.main;
import javax.xml.ws.Endpoint;

import com.epam.exchange.currency.service.ExchangeCurrencyServiceImpl;
public class Main {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8080/ExchangeCurrenciesService", new ExchangeCurrencyServiceImpl());
	}

}

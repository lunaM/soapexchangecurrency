package com.epam.exchange.currency.util;


import java.util.Collection;

import com.epam.exchange.currency.model.Currency;

public class CurrencyExchanger {
	private String date;
	private String base;
	private Collection<Currency> currenciesRate;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public Collection<Currency> getCurrenciesRate() {
		return currenciesRate;
	}
	public void setCurrenciesRate(Collection<Currency> currenciesRate) {
		this.currenciesRate = currenciesRate;
	}
	public CurrencyExchanger() {
	}
	public CurrencyExchanger(String date, String base, Collection<Currency> currenciesRate) {
		super();
		this.date = date;
		this.base = base;
		this.currenciesRate = currenciesRate;
	}
	
	
}

package com.epam.exchange.currency.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.epam.exchange.currency.util.CurrencyExchanger;

@WebService
@SOAPBinding
public interface ExchangeCurrencyService {
	@WebMethod
	CurrencyExchanger getAllByDate();
	@WebMethod
	CurrencyExchanger getAllByDateAndBase(String base, String date);
	@WebMethod
	CurrencyExchanger getCustomByDateAndBase(String base, String date, String currencies);
}

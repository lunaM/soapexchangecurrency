package com.epam.exchange.currency.service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.epam.exchange.currency.model.Currency;
import com.epam.exchange.currency.model.CurrencyProducer;
import com.epam.exchange.currency.util.CurrencyExchanger;

@WebService
public class ExchangeCurrencyServiceImpl implements ExchangeCurrencyService {
	@WebMethod
	public CurrencyExchanger getAllByDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		CurrencyProducer currencyProducer = new CurrencyProducer(dateFormat.format(date));
		CurrencyExchanger exchanger = new CurrencyExchanger();
		exchanger.setBase("UAH");
		exchanger.setDate(currencyProducer.getDate());
		exchanger.setCurrenciesRate(currencyProducer.transformHtmlDataIntoCurrecyObj().values());
        return exchanger;
	}

	public CurrencyExchanger getAllByDateAndBase(String base, String date) {
		CurrencyProducer currencyProducer = new CurrencyProducer(date);
		CurrencyExchanger exchanger = new CurrencyExchanger();
		exchanger.setBase(base);
		exchanger.setDate(currencyProducer.getDate());
		exchanger.setCurrenciesRate(currencyProducer.changedBaseResult(base).values());
        return exchanger;
	}

	public CurrencyExchanger getCustomByDateAndBase(String base, String date, String currencies) {
		CurrencyProducer currencyProducer = new CurrencyProducer(date);
		CurrencyExchanger exchanger = new CurrencyExchanger();
		exchanger.setBase(base);
		exchanger.setDate(currencyProducer.getDate());
		List<String> interestedCurencies = Arrays.asList(currencies.split(","));
		Collection<Currency> allCurrencies = currencyProducer.changedBaseResult(base).values();
		Iterator<Currency> iterator = allCurrencies.iterator();
		while(iterator.hasNext()){
			Currency current = iterator.next();
			if(!interestedCurencies.contains(current.getIdString())){
				iterator.remove();
			}
		}
		exchanger.setCurrenciesRate(allCurrencies);
        return exchanger;
	}

}

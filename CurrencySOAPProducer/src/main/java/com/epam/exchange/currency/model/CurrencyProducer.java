package com.epam.exchange.currency.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CurrencyProducer {
	private String url = "http://www.bank.gov.ua/control/uk/curmetal/currency/search?formType=searchFormDate&time_step=daily&outer=table&date=";
	private String date;
	private Map<String, Currency> currencies;

	public CurrencyProducer(String date) {
		this.date = date;
		url += date;
	}

	public CurrencyProducer() {
	}

	public Map<String, Currency> transformHtmlDataIntoCurrecyObj() {
		int amount = 0;
		try {
			Document doc = Jsoup.connect(url).get();
			Elements elements = doc.select(".cell_c");
			Elements currencyNames = doc.select(".cell");
			amount = elements.size() / 4;
			currencies = new HashMap<String, Currency>(amount);
			Iterator<Element> iterator = elements.iterator();
			Iterator<Element> iteratorNames = currencyNames.iterator();
			for (int j = 0; j < amount; j++) {
				Currency currency = new Currency();
				currency.setIdNumb(Integer.valueOf(iterator.next().text()));
				currency.setIdString(iterator.next().text());
				currency.setAmount(Integer.valueOf(iterator.next().text()));
				currency.setExchangeRate(Double.valueOf(iterator.next().text()));
				currency.setName(iteratorNames.next().text());
				currencies.put(currency.getIdString(), currency);
			}
			Currency currency = new Currency();
			currency.setIdNumb(1);
			currency.setIdString("UAH");
			currency.setAmount(1);
			currency.setExchangeRate(1);
			currency.setName("��������� ������");
			currencies.put(currency.getIdString(), currency);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return currencies;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Map<String, Currency> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(Map<String, Currency> currencies) {
		this.currencies = currencies;
	}

	public Map<String, Currency> changedBaseResult(String baseStr) {
		transformHtmlDataIntoCurrecyObj();
		Currency base = currencies.get(baseStr);
		double oneBaseAgainstUAH = base.getAmount() * 1.0 / base.getExchangeRate();
		for (Currency currency : currencies.values()) {
			currency.setExchangeRate(oneBaseAgainstUAH * currency.getExchangeRate());
		}
		return currencies;
	}
}

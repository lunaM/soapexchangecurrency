package com.epam.exchange.currency.model;

public class Currency {
	private int idNumb;
	private String idString;
	private String name;
	private int amount;
	private double exchangeRate;
	
	public Currency(int idNumb, String idString, String name, int amount, double exchangeRate) {
		super();
		this.idNumb = idNumb;
		this.idString = idString;
		this.name = name;
		this.amount = amount;
		this.exchangeRate = exchangeRate;
	}
	public Currency() {
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public String getIdString() {
		return idString;
	}
	public void setIdString(String idString) {
		this.idString = idString;
	}
	public int getIdNumb() {
		return idNumb;
	}
	public void setIdNumb(int idNumb) {
		this.idNumb = idNumb;
	}
	@Override
	public String toString() {
		return "Currency [idNumb=" + idNumb + ", idString=" + idString + ", name=" + name + ", amount=" + amount
				+ ", exchangeRate=" + exchangeRate + "]";
	}
	
}

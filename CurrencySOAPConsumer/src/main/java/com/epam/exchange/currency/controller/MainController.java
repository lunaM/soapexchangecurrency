package com.epam.exchange.currency.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.epam.exchange.currency.service.Currency;
import com.epam.exchange.currency.service.CurrencyExchanger;
import com.epam.exchange.currency.service.ExchangeCurrencyServiceImpl;
import com.epam.exchange.currency.service.ExchangeCurrencyServiceImplService;

@Component
@Path("/")
public class MainController {
	
	@Path("/ua")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CurrencyExchanger getAllByDate() {
		ExchangeCurrencyServiceImpl webService = new ExchangeCurrencyServiceImplService().getExchangeCurrencyServiceImplPort();
        return webService.getAllByDate();
    }
	
	@Path("/custom/{base}/{date}/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CurrencyExchanger getAllByDateAndBase(@PathParam("base") String base, @PathParam("date") String date) {
		ExchangeCurrencyServiceImpl webService = new ExchangeCurrencyServiceImplService().getExchangeCurrencyServiceImplPort();
        return webService.getAllByDateAndBase(base, date);
    }
	
	@Path("/custom/{base}/{date}/{currecies}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public CurrencyExchanger getCustomByDateAndBase(@PathParam("base") String base, @PathParam("date") String date, @PathParam("currecies") String currencies) {
		ExchangeCurrencyServiceImpl webService = new ExchangeCurrencyServiceImplService().getExchangeCurrencyServiceImplPort();
        return webService.getCustomByDateAndBase(base, date, currencies);
    }
}
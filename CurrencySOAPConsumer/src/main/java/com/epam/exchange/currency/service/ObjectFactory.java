
package com.epam.exchange.currency.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.exchange.currency.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllByDateResponse_QNAME = new QName("http://service.currency.exchange.epam.com/", "getAllByDateResponse");
    private final static QName _GetAllByDateAndBase_QNAME = new QName("http://service.currency.exchange.epam.com/", "getAllByDateAndBase");
    private final static QName _GetAllByDateAndBaseResponse_QNAME = new QName("http://service.currency.exchange.epam.com/", "getAllByDateAndBaseResponse");
    private final static QName _GetCustomByDateAndBase_QNAME = new QName("http://service.currency.exchange.epam.com/", "getCustomByDateAndBase");
    private final static QName _GetAllByDate_QNAME = new QName("http://service.currency.exchange.epam.com/", "getAllByDate");
    private final static QName _GetCustomByDateAndBaseResponse_QNAME = new QName("http://service.currency.exchange.epam.com/", "getCustomByDateAndBaseResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.exchange.currency.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllByDateAndBaseResponse }
     * 
     */
    public GetAllByDateAndBaseResponse createGetAllByDateAndBaseResponse() {
        return new GetAllByDateAndBaseResponse();
    }

    /**
     * Create an instance of {@link GetCustomByDateAndBase }
     * 
     */
    public GetCustomByDateAndBase createGetCustomByDateAndBase() {
        return new GetCustomByDateAndBase();
    }

    /**
     * Create an instance of {@link GetAllByDate }
     * 
     */
    public GetAllByDate createGetAllByDate() {
        return new GetAllByDate();
    }

    /**
     * Create an instance of {@link GetCustomByDateAndBaseResponse }
     * 
     */
    public GetCustomByDateAndBaseResponse createGetCustomByDateAndBaseResponse() {
        return new GetCustomByDateAndBaseResponse();
    }

    /**
     * Create an instance of {@link GetAllByDateResponse }
     * 
     */
    public GetAllByDateResponse createGetAllByDateResponse() {
        return new GetAllByDateResponse();
    }

    /**
     * Create an instance of {@link GetAllByDateAndBase }
     * 
     */
    public GetAllByDateAndBase createGetAllByDateAndBase() {
        return new GetAllByDateAndBase();
    }

    /**
     * Create an instance of {@link CurrencyExchanger }
     * 
     */
    public CurrencyExchanger createCurrencyExchanger() {
        return new CurrencyExchanger();
    }

    /**
     * Create an instance of {@link Currency }
     * 
     */
    public Currency createCurrency() {
        return new Currency();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllByDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.currency.exchange.epam.com/", name = "getAllByDateResponse")
    public JAXBElement<GetAllByDateResponse> createGetAllByDateResponse(GetAllByDateResponse value) {
        return new JAXBElement<GetAllByDateResponse>(_GetAllByDateResponse_QNAME, GetAllByDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllByDateAndBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.currency.exchange.epam.com/", name = "getAllByDateAndBase")
    public JAXBElement<GetAllByDateAndBase> createGetAllByDateAndBase(GetAllByDateAndBase value) {
        return new JAXBElement<GetAllByDateAndBase>(_GetAllByDateAndBase_QNAME, GetAllByDateAndBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllByDateAndBaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.currency.exchange.epam.com/", name = "getAllByDateAndBaseResponse")
    public JAXBElement<GetAllByDateAndBaseResponse> createGetAllByDateAndBaseResponse(GetAllByDateAndBaseResponse value) {
        return new JAXBElement<GetAllByDateAndBaseResponse>(_GetAllByDateAndBaseResponse_QNAME, GetAllByDateAndBaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomByDateAndBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.currency.exchange.epam.com/", name = "getCustomByDateAndBase")
    public JAXBElement<GetCustomByDateAndBase> createGetCustomByDateAndBase(GetCustomByDateAndBase value) {
        return new JAXBElement<GetCustomByDateAndBase>(_GetCustomByDateAndBase_QNAME, GetCustomByDateAndBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllByDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.currency.exchange.epam.com/", name = "getAllByDate")
    public JAXBElement<GetAllByDate> createGetAllByDate(GetAllByDate value) {
        return new JAXBElement<GetAllByDate>(_GetAllByDate_QNAME, GetAllByDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomByDateAndBaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.currency.exchange.epam.com/", name = "getCustomByDateAndBaseResponse")
    public JAXBElement<GetCustomByDateAndBaseResponse> createGetCustomByDateAndBaseResponse(GetCustomByDateAndBaseResponse value) {
        return new JAXBElement<GetCustomByDateAndBaseResponse>(_GetCustomByDateAndBaseResponse_QNAME, GetCustomByDateAndBaseResponse.class, null, value);
    }

}
